1. Installation
2. Testing

/*****************
 * 1. Installation
 ****************/
- Step 1
Enable the module and make sure the wincache extension is installed properly on
the status page (http://yoursite/admin/reports/status).

Step two is important to think about because it can make your site faster
or slower depending on the right configuration.

- Step 2a
Add the following code to your settings.php file:

/**
 * Add Win Caching.
 */
$conf['cache_backends'] = 
  array('sites/all/modules/drupalwincache/drupal_win_cache.inc');
$conf['cache_class_cache'] = 'DrupalWinCache';
$conf['cache_class_cache_bootstrap'] = 'DrupalWinCache';
// Remove the slashes to use debug mode.
// $conf['wincache_show_debug'] = TRUE;

- Step 2b
Add the following code to your settings.php file:

/**
 * Add Win Caching.
 */
$conf['cache_backends'] = 
  array('sites/all/modules/drupalwincache/drupal_win_cache.inc');
$conf['cache_default_class'] = 'DrupalWinCache';
// Remove the slashes to use debug mode.
//$conf['wincache_show_debug'] = TRUE;

- Step 3
Visit your site to see or it's still working!

- Step 4 (OPTIONAL)
When using DrupalWinCache as default or manually caching the 'cache_page' bin
in your settings file you do not need to start the database because Drupal can
use the WinCache for pages. Add the following code to your settings.php file
to do so:

$conf['page_cache_without_database'] = TRUE;
$conf['page_cache_invoke_hooks'] = FALSE;

- Step 5 (OPTIONAL)
Visit your site to see or it's still working!


/*****************
 * 2. Testing
 ****************/
To be able to test this module open DRUPAL_ROOT/includes/cache.inc and search
for `variable_get('cache_default_class', 'DrupalDatabaseCache')`. and change
this to DrupalWinCache. This is because the $conf[''] array in settings.php
is not always loaded properly.
